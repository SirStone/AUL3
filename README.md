<img src="media/AUL-Logo.png" width=65%>

# AUL3 \(Arma Utility Library)

AUL3 is a library containing utility functions used to build custom launchers and server management tools for Arma 3.

While custom launchers are mostly used on Linux, this library should work for Windows and Mac as well.

### Programs Using AUL3:

None

### Usage:

Place the header in your project's header folder and the library in your project's library folder, then include it where you see fit, nothing else.

### Notes:

This is a library, not a program, if you somehow got here though search engine results while looking for a launcher, check the [Programs Using AUL3](#programs-using-aul3) section.

If you are a developer using this library to develop your own launcher or tool, let me know so I can add it to the [Programs Using AUL3](#programs-using-aul3) section.

### Available Wrappings:

None
