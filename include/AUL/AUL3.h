#pragma once
// MIT License
// 
// Copyright (c) 2020 Stein
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <dirent.h>

// Switch all functions to use dynamic allocation using create and destroy functions.


/// This is the datatype for mod metadata which includes mod_name, workshop_id and workshop_url
typedef struct aul_ModData {
	char *mod_name; /// The mod display name
	char *workshop_id; /// The workshop ID, stored as a character array to avoid making a hard 64-bit dependency
	char *workshop_url; /// The workshop URL
}aul_ModData;

aul_ModData* aul_CreateModData(const char *mod_name, const char *workshop_id, const char *workshop_url);
void aul_DestroyModData(aul_ModData *modData);

typedef struct aul_PresetData {
	char *preset_name; /// Name of the preset, used for preset identification
	aul_ModData *mods; /// An array of mods containing mod metadata
	uint16_t modCount;
}aul_PresetData;

aul_PresetData* aul_CreatePresetData(const char *preset_name, aul_ModData *mod_data);

/// Destroys preset data and also any mod data contained
void aul_DestroyPresetData(aul_PresetData *preset_data);

/// Stores launch parameters
typedef struct aul_Parameters {
	bool enableHT; /// enable hyper threading
	bool host; /// host non-dedicated server on startup
	bool hugepages; /// enable huge pages
	bool noCB; /// disable multicore use of GPU
	bool noLogs; /// disable logging
	bool noPause; /// keeps game running regardless of focus
	bool noSplash; /// disable splash screen
	bool skipIntro;
	bool windowed; /// start in windowed mode
	const char* connect; /// connect to <IP> on startup
	const char* password; /// uses <password> when connecting to server on startup
	const char* port; /// <port> to use when connecting to server on startup
	const char* profiles; /// profiles location
	int adapter; /// screen adapter to use
	int cpuCount;
	int exThreads; /// define extra threads for geometry loading, texture loading and file operations, valid values are 0, 1, 3, 5 and 7
	int maxMem; /// maximum memory arma will try not to exceed
	int maxVRAM; /// maximum VRAM arma will try not to exceed

	// note: the -nolauncher flag is not included here, aul_LaunchArma() will automatically add this flag to launch parameters
}aul_Parameters;

enum aul_DirType {
	GAME_DIR=0,
	WORKSHOP_DIR=1
};

/// ModLauncherList entries
typedef struct aul_ModLauncherList {
	char class_name[64];
	char directory[64];
	enum aul_DirType origin; /// origin point of directory
	char full_path[512];
	struct aul_ModLauncherList *sub_class;
}aul_ModLauncherList;

/// Gets substring from string, remember to free string
static char* getSubString(const char* delim_start, const char* delim_end, const char* line);

/// Parses HTML mod lists from the official launcher
aul_PresetData* aul_CreateFromPreset(const char *file_path);

/// Generates an HTML mod list that is compatible with the official launcher
void aul_GeneratePreset(aul_PresetData* preset, const char* preset_file);

/// Gets an array of all subscribed mods from the workshop directory
aul_ModData* aul_GetSubscribedMods(const char* workshop_dir, uint16_t* mod_count);

/// Downloads a mod by the given workshop_id to the target directory, if update_flag is non-zero the downloaded mod will be updated
int aul_DownloadMod(const char* workshop_id, const char* target_directory, bool update);

/// Gets active mods from an Arma3.cfg file and return workshop IDs. NOTE: The Arma3.cfg file must have it's ModLauncherList class either be removed or be using the WORKSHOP DIR format
aul_ModData* aul_GetActiveMods(const char *config_path, uint16_t* mod_count);

/// Sets active mods in an Arma3.cfg file
void aul_SetActiveMods(const char *config_path, char *workshop_path, aul_ModData *mod_data, int mod_count);

int aul_AddModToPreset(aul_PresetData* preset_data, aul_ModData mod_data);

int aul_RemoveModFromPreset(aul_PresetData *preset, const char* mod_id);

/// Launch Arma 3 with given parameters
int aul_LaunchArma();
