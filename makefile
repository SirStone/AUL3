INCLUDES = -I include
CFLAGS = -g -Wall -Wl,-rpath='.'
LIBS = -lAUL
BUILD_PATH = build
SRC_PATH = src
CC = gcc ${CFLAGS} ${INCLUDES} -L ${BUILD_PATH}

SOURCE_LIB := $(SRC_PATH)/aul.c
SOURCE_BIN := $(SRC_PATH)/aul_cli.c
OBJECTS_LIB := $(SOURCE_LIB:$(SRC_PATH)/%.c=$(BUILD_PATH)/objects/%.o)
OBJECTS_BIN := $(SOURCE_BIN:$(SRC_PATH)/%.c=$(BUILD_PATH)/objects_bin/%.o)

CMD_PREFIX := @

.DEFAULT: all
all: dirs $(BUILD_PATH)/libAUL.so $(BUILD_PATH)/aulcli

.PHONY: clean
clean:
	@echo "Cleaning up build files..."
	@rm -rf $(BUILD_PATH)

.PHONY: dirs
dirs:
	@echo "Creating directories..."
	@mkdir -p $(BUILD_PATH)
	@mkdir -p $(BUILD_PATH)/objects
	@mkdir -p $(BUILD_PATH)/objects_bin

$(OBJECTS_LIB): $(SOURCE_LIB)
	@echo "Compiling: $< -> $@..."
	$(CMD_PREFIX)$(CC) -c $< -o $@
	@echo "Finished compiling."

$(OBJECTS_BIN): $(SOURCE_BIN)
	@echo "Compiling: $< -> $@..."
	$(CMD_PREFIX)$(CC) -c $< -o $@
	@echo "Finished compiling."

$(BUILD_PATH)/libAUL.so: $(OBJECTS_LIB)
	@echo "Linking: $@..."
	$(CMD_PREFIX)$(CC) -shared $(OBJECTS_LIB) -o $@
	@echo "Finished linking."

$(BUILD_PATH)/aulcli: $(OBJECTS_BIN)
	@echo "Linking: $@..."
	$(CMD_PREFIX)$(CC) $(OBJECTS_BIN) $(LIBS) -o $@
	@echo "Finished linking."
