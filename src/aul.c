#include "AUL/AUL3.h"

static const char* aul_PresetTemplate =
"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
"<html>\n"
"  <!--Created by AUL3: https://gitlab.com/SirStone/AUL3-->\n"
"  <head>\n"
"    <meta name=\"arma:Type\" content=\"preset\" />\n"
"    <meta name=\"arma:PresetName\" content=\"%s\" />\n"
"    <meta name=\"generator\" content=\"Arma 3 Launcher - https://arma3.com\" />\n"
"    <title>Arma 3</title>\n"
"    <link href=\"https://fonts.googleapis.com/css?family=Roboto\" rel=\"stylesheet\" type=\"text/css\" />\n"
"    <style>\n"
"body {\n"
"	margin: 0;\n"
"	padding: 0;\n"
"	color: #fff;\n"
"	background-image: linear-gradient(180deg, #101011, #404044);\n"
"}\n"
"\n"
"body, th, td {\n"
"	font: 95%/1.3 Roboto, Segoe UI, Tahoma, Arial, Helvetica, sans-serif;\n"
"}\n"
"\n"
"td {\n"
"	display: table-cell;\n"
"	background: #101010;\n"
"	padding: 25px;\n"
"	padding-left: 0;\n"
"	padding-right: 0;\n"
"	text-align: center;\n"
"	vertical-align: middle;\n"
"	font-size: 25px;\n"
"}\n"
"\n"
"td a {\n"
"	visibility: hidden;\n"
"	padding: 25px;	\n"
"	margin-left: 200px;\n"
"	text-decoration: none;\n"
"	font-size: 0px;\n"
"}\n"
"\n"
"td a:before {\n"
"	content: \"Link\";\n"
"	text-decoration: none;\n"
"	visibility: visible;\n"
"	display: block;\n"
"	font-size: 25px;\n"
"}\n"
"\n"
"h1 {\n"
"    padding: 20px 20px 0 20px;\n"
"    color: white;\n"
"    font-weight: 200;\n"
"    font-family: segoe ui;\n"
"    font-size: 3em;\n"
"    margin-left: 0;\n"
"	text-align: center;\n"
"}\n"
"\n"
"em {\n"
"    font-variant: italic;\n"
"    color:silver;\n"
"}\n"
"\n"
"table {\n"
"	margin-left: auto;\n"
"	margin-right: auto;\n"
"	width: auto;\n"
"	padding: 50px;\n"
"	background: #222222;\n"
"}\n"
"\n"
".before-list {\n"
"	display: block;\n"
"    padding: 5px 20px 10px 20px;\n"
"	text-align: center;\n"
"	background: black;\n"
"	margin: 0;\n"
"	width: 100%;\n"
"}\n"
"\n"
".mod-list {\n"
"    background: transparent;\n"
"    padding: 20px;\n"
"}\n"
"\n"
".dlc-list {\n"
"    background: #222222;\n"
"    padding: 20px;\n"
"}\n"
"\n"
".footer {\n"
"    padding: 20px;\n"
"    color:gray;\n"
"}\n"
"\n"
".whups {\n"
"    color:gray;\n"
"}\n"
"\n"
"a {\n"
"    color: #D18F21;\n"
"    text-decoration: underline;\n"
"}\n"
"\n"
"a:hover {\n"
"    color:#F1AF41;\n"
"    text-decoration: none;\n"
"}\n"
"\n"
".from-steam {\n"
"    color: #449EBD;\n"
"	display: none;\n"
"}\n"
".from-local {\n"
"    color: gray;\n"
"}\n"
"\n"
"</style>\n"
"  </head>\n"
"  <body>\n"
"    <h1>Arma 3  - Preset <strong>%s</strong></h1>\n"
"    <p class=\"before-list\">\n"
"      <em>Drag this file or link to it to Arma 3 Launcher or open it Mods / Preset / Import.</em>\n"
"    </p>\n"
"    <div class=\"mod-list\">\n"
"      <table>\n"
"        %s"
"      </table>\n"
"    </div>\n"
"	<div class=\"before-list\">\n"
"      <em>%i Mods Total</em>\n"
"    </div>\n"
"    <div class=\"dlc-list\">\n"
"      <table />\n"
"    </div>	\n"
"    <div class=\"footer\">\n"
"      <span>Created by AUL3 by Stein.</span>\n"
"    </div>\n"
"  </body>\n"
"</html>";

static const char* aul_PresetEntryTemplate =
"<tr data-type=\"ModContainer\">\n"
"          <td data-type=\"DisplayName\">%s</td>\n"
"          <td>\n"
"            <span class=\"from-steam\">Steam</span>\n"
"          </td>\n"
"          <td>\n"
"            <a href=\"%s\" data-type=\"Link\">%s</a>\n"
"          </td>\n"
"        </tr>\n";


// Launch command figures out how to launch, where to launch and verify given arguments
int aul_LaunchArma() {
	
	char cmd[] = "steam -applaunch 107410 -profiles=Profiles";
	system(cmd);
	return 0;
}

aul_PresetData* aul_CreateFromPreset(const char *file_path) {
	FILE *preset_file = fopen(file_path, "r");

	if (preset_file == NULL) {
		printf("AUL3 Error: Could not open file \"%s\"\n", file_path);
		return NULL;
	}

	aul_PresetData *preset = malloc(sizeof(aul_PresetData));
	preset->mods = malloc(sizeof(aul_ModData));
	preset->modCount = 1;	
	
	while(1) {
		char *res = NULL;
		char line[255];
		if (fgets(line, 255, preset_file) == NULL) {
			break;
		}
		else {
			res = getSubString("<strong>", "</strong>", line);
			if (res != NULL) {
				preset->preset_name = malloc(strlen(res)+1);
				strcpy(preset->preset_name, res);
				free(res);
				continue;
			}
			res = getSubString("data-type=\"DisplayName\">", "</td>", line);
			if (res != NULL) {	
				preset->mods[preset->modCount-1].mod_name = malloc(strlen(res)+1);
				strcpy(preset->mods[preset->modCount-1].mod_name, res);	
				free(res);
				continue;
			}
			res = getSubString("data-type=\"Link\">", "</a>", line);
			if (res != NULL) {
				preset->mods[preset->modCount-1].workshop_url = malloc(strlen(res)+1);
				strcpy(preset->mods[preset->modCount-1].workshop_url, res);

				char *res_id = getSubString("?id=", NULL, res);
				if (res_id != NULL) {
					preset->mods[preset->modCount-1].workshop_id = malloc(strlen(res_id)+1);
					strcpy(preset->mods[preset->modCount-1].workshop_id, res_id);
					free(res_id);
				}
				else {
					preset->mods[preset->modCount-1].workshop_id = malloc(sizeof(char)*5);
					strcpy(preset->mods[preset->modCount-1].workshop_id, "None");
				}
				free(res);
				preset->mods = (aul_ModData*) realloc(preset->mods, sizeof(aul_ModData) * (preset->modCount+1));
				preset->modCount++;
				preset->mods[preset->modCount-1].mod_name = NULL;
				preset->mods[preset->modCount-1].workshop_id = NULL;
				preset->mods[preset->modCount-1].workshop_url = NULL;
				continue;
			}
		}
	}
	fclose(preset_file);
	preset->modCount -= 1;
	return preset;
}

aul_ModData* aul_GetSubscribedMods(const char* workshop_dir, uint16_t* mod_count) {
	DIR *dir;
	struct dirent *ent;	
	if ((dir = opendir(workshop_dir)) != NULL) {
		aul_ModData *mod_data = malloc(sizeof(aul_ModData));
		int m_count = 1;
		while ((ent = readdir(dir)) != NULL) {
			// try to find a mod.cpp file to get the name of the mod
			FILE *mod_cpp = NULL;
			char mod_path[512] = {};
			strcat(mod_path, workshop_dir);
			strcat(mod_path, ent->d_name);
			strcat(mod_path, "/meta.cpp");
			mod_cpp = fopen(mod_path, "r");
			if (mod_cpp != NULL) {
				while(1) {
					char *res = NULL;
					char line[255];
					if (fgets(line, 255, mod_cpp) == NULL) {
						break;
					}
					else {
						res = getSubString("name = \"", "\";", line);
						if (res != NULL) {
							char mod_url[512] = "https://steamcommunity.com/workshop/filedetails/?id=";
							strcat(mod_url, ent->d_name);
							mod_data[m_count-1].mod_name = malloc(sizeof(char) * (strlen(res) + 2));
							strcpy(mod_data[m_count-1].mod_name, res);
							mod_data[m_count-1].workshop_url = malloc(sizeof(char) * (strlen(mod_url) + 1));
							strcpy(mod_data[m_count-1].workshop_url, mod_url);
							mod_data[m_count-1].workshop_id = malloc(sizeof(char) * (strlen(ent->d_name) + 1));
							strcpy(mod_data[m_count-1].workshop_id, ent->d_name);
							free(res);
							mod_data = (aul_ModData*) realloc(mod_data, sizeof(aul_ModData) * (m_count+1));
							m_count = m_count + 1;
							mod_data[m_count-1].mod_name = NULL;
							mod_data[m_count-1].workshop_id = NULL;
							mod_data[m_count-1].workshop_url = NULL;		
						}	
					}
				}
				fclose(mod_cpp);
			}
		}
		m_count--;
		if (mod_count != NULL) {
			*mod_count = m_count;
		}
		closedir(dir);
		return mod_data;
	}
	else {
		printf("AUL3 Error: Could not open directory \"%s\"\n", workshop_dir);
		return NULL;
	}
}

aul_ModData* aul_GetActiveMods(const char *config_path, uint16_t* mod_count) {

	FILE *cfg_file = fopen(config_path, "r");

	if (cfg_file == NULL) {
		printf("AUL3 Error: Could not open file \"%s\"\n", config_path);
		return NULL;
	}

	aul_ModData *mod_data = NULL;
	mod_data = malloc(sizeof(aul_ModData));
	int m_count = 0;
	
	while(1) {
		char *res = NULL;
		char line[255];
		if (fgets(line, 255, cfg_file) == NULL) {
			break;
		}
		else {
			res = getSubString("dir=\"", "\";", line);
			if (res != NULL) {
				mod_data[m_count].workshop_id = malloc(strlen(res)+1);
				strcpy(mod_data[m_count].workshop_id, res);
				char url[128] = "https://steamcommunity.com/workshop/filedetails/?id=";
				strcat(url, res);
				mod_data[m_count].workshop_url = malloc(strlen(url)+1);
				strcpy(mod_data[m_count].workshop_url, url);
				free(res);
				continue;
			}
			res = getSubString("name=\"", "\";", line);
			if (res != NULL) {
				mod_data[m_count].mod_name = malloc(strlen(res)+1);
				strcpy(mod_data[m_count].mod_name, res);
				m_count = m_count + 1;
				mod_data = (aul_ModData*) realloc(mod_data, sizeof(aul_ModData) * (m_count+1));
				free(res);
				continue;
			}
		}
	}
	m_count--;
	if (mod_count != NULL) {
		*mod_count = m_count;
	}
	fclose(cfg_file);
	return mod_data;
	
}

void aul_SetActiveMods(const char *config_path, char *workshop_path, aul_ModData* mod_data, int mod_count) {
	FILE *cfg_file = fopen(config_path, "r");
	FILE *cfg_file_buffer = fopen("/tmp/Arma3.cfg.buffer", "w+");


	if (cfg_file == NULL) {
		printf("AUL3 Error: Could not open file \"%s\"\n", config_path);
		return;
	}

	while(1) {
		char line[255];
		if (fgets(line, 255, cfg_file) == NULL) {
			break;
		}
		else {
			if (strncmp(line, "class ModLauncherList", sizeof(char) * 21) == 0) {
				// Start deleting lines until the }; is reached (no starting whitespace)
				while(1) {
					if (strncmp(line, "};", sizeof(char) * 2) == 0) {
						if (fgets(line, 255, cfg_file) == NULL) {
							break;
						}
						break;
					}
					if (fgets(line, 255, cfg_file) == NULL) {
						break;
					}
				}
				while(fgets(line, 255, cfg_file) != NULL) {
					fputs(line, cfg_file_buffer);
				}
			}
			else {
				fputs(line, cfg_file_buffer);
			}
		}
	}

	fclose(cfg_file);
	fclose(cfg_file_buffer);

	char line[255];
	cfg_file = fopen(config_path, "w+");
	cfg_file_buffer = fopen("/tmp/Arma3.cfg.buffer", "r");
	while(fgets(line, 255, cfg_file_buffer) != NULL) {
		fputs(line, cfg_file);
	}
	fclose(cfg_file_buffer);
	remove("/tmp/Arma3.cfg.buffer");

	if (mod_data != NULL) {
		fputs("class ModLauncherList\n{\n", cfg_file);
		// Fix / paths with backslash
		for (int i=0; i<strlen(workshop_path); i++) {
			if (workshop_path[i] == '/') {
				workshop_path[i] = '\\';
			}
		}
	
		for (int i=0; i<mod_count; i++) {
			fprintf(cfg_file, "\tclass Mod%i\n\t{\n"
							"\t\tdir=\"%s\";\n"
							"\t\tname=\"%s\";\n"
							"\t\torigin=\"WORKSHOP DIR\";\n"
							"\t\tfullPath=\"Z:%s%s\";\n"
							"\t};\n",
							i+1,
							mod_data[i].workshop_id,
							mod_data[i].mod_name,
							workshop_path,
							mod_data[i].workshop_id
							);
			printf("Setting active mod: %s\n", mod_data[i].mod_name);
		}
		// Adding new line to prevent remaining }; when deleting
		fputs("};", cfg_file);
		fclose(cfg_file);
		return;
	}
	fclose(cfg_file);
}

int aul_AddModToPreset(aul_PresetData* preset_data, aul_ModData mod_data) {

	for (int i=0; i<preset_data->modCount; i++) {
		if (strcmp(mod_data.workshop_id, preset_data->mods[i].workshop_id) == 0) {
			return -1;
		}
	}
	preset_data->mods = (aul_ModData*) realloc(preset_data->mods, sizeof(aul_ModData) * (preset_data->modCount+1));
	preset_data->modCount++;
	preset_data->mods[preset_data->modCount-1].mod_name = malloc(strlen(mod_data.mod_name)+1);
	strcpy(preset_data->mods[preset_data->modCount-1].mod_name, mod_data.mod_name);
	preset_data->mods[preset_data->modCount-1].workshop_id = malloc(strlen(mod_data.workshop_id)+1);
	strcpy(preset_data->mods[preset_data->modCount-1].workshop_id, mod_data.workshop_id);
	preset_data->mods[preset_data->modCount-1].workshop_url = malloc(strlen(mod_data.workshop_url)+1);
	strcpy(preset_data->mods[preset_data->modCount-1].workshop_url, mod_data.workshop_url);
	return 0;
}

int aul_RemoveModFromPreset(aul_PresetData *preset, const char* mod_id) {
	
	aul_ModData *new_mods = (aul_ModData*) malloc(sizeof(aul_ModData) * (preset->modCount-1));
	int new_mods_count = 0;
	int success = 1;

	for (int i=0; i<preset->modCount; i++) {
		if (strcmp(preset->mods[i].workshop_id, mod_id) == 0) {
			success = 0;
			continue;
		}
		else {
			new_mods[new_mods_count].mod_name = malloc(strlen(preset->mods[new_mods_count].mod_name)+1);
			strcpy(new_mods[new_mods_count].mod_name, preset->mods[i].mod_name);

			new_mods[new_mods_count].workshop_id = malloc(strlen(preset->mods[new_mods_count].workshop_id)+1);
			strcpy(new_mods[new_mods_count].workshop_id, preset->mods[i].workshop_id);

			new_mods[new_mods_count].workshop_url = malloc(strlen(preset->mods[new_mods_count].workshop_url)+1);
			strcpy(new_mods[new_mods_count].workshop_url, preset->mods[i].workshop_url);

			new_mods_count++;
		}
	}
	aul_DestroyModData(preset->mods);
	preset->mods = new_mods;
	preset->modCount--;
	return success;
}

void aul_GeneratePreset(aul_PresetData* preset, const char* preset_file) {
	FILE* p_file = fopen(preset_file, "w+");
	char buffer[262144]; // 2mb buffer
	char entry_buffer[2048];

	for (int i=0; i<preset->modCount; i++) {
		snprintf(entry_buffer, sizeof(buffer), aul_PresetEntryTemplate, preset->mods[i].mod_name, preset->mods[i].workshop_url, preset->mods[i].workshop_url);
		strcat(buffer, entry_buffer);
	}
	fprintf(p_file, aul_PresetTemplate, preset->preset_name, preset->preset_name, buffer, preset->modCount);
	fclose(p_file);
}

aul_ModData* aul_CreateModData(const char *mod_name, const char *workshop_id, const char *workshop_url) {
	aul_ModData *data = NULL;

	data = malloc(sizeof(aul_ModData));
	data->mod_name = malloc(strlen(mod_name)+1);
	data->workshop_id = malloc(strlen(workshop_id)+1);

	if (workshop_url != NULL) {
		data->workshop_url = malloc(strlen(workshop_url)+1);
		strcpy(data->workshop_url, workshop_url);
	}
	else {
		char url[128] = "https://steamcommunity.com/workshop/filedetails/?id=";
		strcat(url, workshop_id);
		data->workshop_url = malloc(strlen(url)+1);
		strcpy(data->workshop_url, url);
	}

	strcpy(data->mod_name, mod_name);
	strcpy(data->workshop_id, workshop_id);	

	return data;
}

void aul_DestroyModData(aul_ModData *data) {
	free(data->workshop_url);
	free(data->workshop_id);
	free(data->mod_name);
	//free(data);
}

aul_PresetData* aul_CreatePresetData(const char *preset_name, aul_ModData *mod_data) {
	aul_PresetData* preset_data = NULL;
	preset_data = malloc(sizeof(aul_PresetData));
	preset_data->preset_name = malloc(strlen(preset_name)+1);
	strcpy(preset_data->preset_name, preset_name);
	preset_data->mods = mod_data;
	int mod_count;
	for (mod_count = 0; mod_data[mod_count].mod_name != NULL; mod_count++);
	preset_data->modCount = mod_count;

	return preset_data;
}

void aul_DestroyPresetData(aul_PresetData *preset_data) {
	for (int i=0; i<preset_data->modCount; i++) {
		aul_DestroyModData(&preset_data->mods[i]);
	}
	preset_data->modCount = 0;
	free(preset_data->preset_name);
	free(preset_data->mods);
	free(preset_data);
}

static char* getSubString(const char* delim_start, const char* delim_end, const char* line) {
	const char *delim_start_position = strstr(line, delim_start);
	if (delim_start_position != NULL) {
		const size_t delim_start_length = strlen(delim_start);
		char *delim_end_position;
		if (delim_end == NULL) {
			delim_end_position = (char*) &line[strlen(line)]; // if no end delim is given, end position is the end of line
		}
		else {
			delim_end_position = strstr(line, delim_end);
		}
		if (delim_end_position != NULL) {
			const size_t substring_length = delim_end_position - (delim_start_position + delim_start_length);
			char *result = malloc(substring_length+1);
			if (result != NULL) {
				memcpy(result, delim_start_position + delim_start_length, substring_length);
				result[substring_length] = '\0';
				return result;
			}
		}
	}
	return NULL;
}
