#include "AUL/AUL3.h"
#include <unistd.h>
#include <getopt.h>

void listMods(const char* path, bool silent_mode) {
	aul_PresetData* preset = aul_CreatePresetData("My Preset", aul_GetSubscribedMods(path, NULL));

	if (!silent_mode) {
		for (int i=0; i<preset->modCount; i++) {
			printf("Mod ID: %s\n"
				"Mod Name: %s\n"
				"Mod URL: %s\n\n",
				preset->mods[i].workshop_id,
				preset->mods[i].mod_name,
				preset->mods[i].workshop_url
				);
		}
		printf("Total mods: %i\n", preset->modCount);
	}
	else {
		for (int i=0; i<preset->modCount; i++) {
			printf("%s\n", preset->mods[i].workshop_id);
		}
	}
	aul_DestroyPresetData(preset);
}

void listActiveMods(const char* config_file, bool silent_mode) {
	aul_PresetData* preset = aul_CreatePresetData("My Preset", aul_GetActiveMods(config_file, NULL));

	if (!silent_mode) {
		for (int i=0; i<preset->modCount; i++) {
			printf("Mod ID: %s\n"
				"Mod Name: %s\n"
				"Mod URL: %s\n\n",
				preset->mods[i].workshop_id,
				preset->mods[i].mod_name,
				preset->mods[i].workshop_url
				);
		}
		printf("Total mods: %i\n", preset->modCount);
	}
	else {
		for (int i=0; i<preset->modCount; i++) {
			printf("%s\n", preset->mods[i].workshop_id);
		}
	}
	aul_DestroyPresetData(preset);
}

void listPreset(const char* preset_file, bool silent_mode) {
	aul_PresetData *preset = aul_CreateFromPreset(preset_file);
	if (!silent_mode) {
		for (int i=0; i<preset->modCount; i++) {
			printf("Mod ID: %s\n"
				"Mod Name: %s\n"
				"Mod URL: %s\n\n",
				preset->mods[i].workshop_id,
				preset->mods[i].mod_name,
				preset->mods[i].workshop_url
				);
		}
		printf("Preset name: %s\n", preset->preset_name);
		printf("Total mods: %i\n", preset->modCount);
	}
	else {
		for (int i=0; i<preset->modCount; i++) {
			printf("%s\n", preset->mods[i].workshop_id);
		}
	}
	aul_DestroyPresetData(preset);
}

void setActiveMods(const char* preset_file, const char* config_file, char* workshop_path) {	
	if (preset_file != NULL) {
		aul_PresetData *preset = aul_CreateFromPreset(preset_file);
		if (preset != NULL) {
			aul_SetActiveMods(config_file, workshop_path, preset->mods, preset->modCount);
			aul_DestroyPresetData(preset);
		}	
	}
	else {
		aul_SetActiveMods(config_file, workshop_path, NULL, 0);
	}	
}

void exportPreset(const char* preset_file, const char* preset_name, const char* config_file) {
	aul_PresetData* preset = aul_CreatePresetData(preset_name, aul_GetActiveMods(config_file, NULL));
	if (preset != NULL) {
		aul_GeneratePreset(preset, preset_file);
		printf("Exported preset.\n");
	}
	else {
		printf("AUL3 CLI Error: Failed to create preset\n");
	}
}

int main(int argc, char* argv[]) {

	// setup environment variables
	char workshop_dir[512] = {};	
	if (getenv("AUL3_WORKSHOP_DIR") != NULL) {
		strcpy(workshop_dir, getenv("AUL3_WORKSHOP_DIR"));
	}
	else {	
		strcat(workshop_dir, getenv("HOME"));
		strcat(workshop_dir, "/.local/share/Steam/steamapps/workshop/content/107410/");
	}

	char config_file[512] = {};	
	if (getenv("AUL3_A3CFG_FILE") != NULL) {
		strcpy(config_file, getenv("AUL3_A3CFG_FILE"));
	}
	else {
		strcat(config_file, getenv("HOME"));
		strcat(config_file, "/.local/share/Steam/steamapps/common/Arma 3/Profiles/Users/steamuser/Arma3.cfg");
	}

	const char* help_message =
			"Usage: aulcli <command> [options]\n\n"
			"Commands/Arguments:\n\n"
			"list [options]                    Lists all mods in workshop directory\n"
			"  -S                              Silent mode, list out workshop ID's only\n"
			"  -a                              Lists all active mods\n"
			"  -l                              Lists all locally installed mods\n"
			"  -p <file>                       Lists all mods found in HTML preset\n"
			"  -x <file> <name>                Exports active mods to preset file\n\n"
			"add [options] [workshopid]        Adds selected mod to active mods\n"
			"  -I <file>                       Imports active mods from preset file\n\n"
			"remove [options] <workshopid>     Remove selected mod from active mods\n"
			"  -R                              Remove all mods from active mods\n\n"
			"search <workshopid>               Searches for selected workshop ID from subscribed mods\n"
			"  -p <file>                       Search in HTML preset for workshop ID\n\n"
			"launch [args]                     Launch Arma 3 with args\n"
			"help                              Prints this help message\n";

	if (argc == 1) {
		printf("%s", help_message);
	}
	else {
		if (strcmp(argv[1], "help") == 0) {
			printf("%s", help_message);
		}
		else if (strcmp(argv[1], "list") == 0) {
			int option;
			bool silent_mode = false;
			while((option = getopt(argc, argv, "alp:Sx:")) != -1) {
				switch(option) {
					case 'S':

						silent_mode = true;
						break;
					case 'a':

						listActiveMods(config_file, silent_mode);
						return 0;
						break;
					case 'l':

						printf("Option not implemented\n");
						return 0;
						break;
					case 'p':

						listPreset(optarg, silent_mode);
						return 0;
						break;	
					case 'x':
						if (optind != argc) {
							exportPreset(optarg, argv[optind], config_file);
							return 0;
						}
						else {
							printf("%s: option requires an argument -- 'x'\n", argv[0]);
							printf("See \'help\' command for help\n");
							return 1;
						}
						break;
					case '?':
						printf("See \'help\' commnand for help\n");
						return 1;
						break;
				}
			}
			listMods(workshop_dir, silent_mode);
		}
		else if (strcmp(argv[1], "add") == 0) {

			if (argc < 3) {
				printf("%s: command requires an argument -- 'add'\n", argv[0]);
				return 1;
			}
			int option;
			while((option = getopt(argc, argv, "I:")) != -1) {
				switch(option) {
					case 'I':
						setActiveMods(optarg, config_file, workshop_dir);
						printf("Imported preset\n");
						return 0;
						break;
					case '?':

						printf("See \'help\' commnand for help\n");
						return 1;
						break;
				}
			}
			aul_PresetData* active_preset = NULL;
			active_preset = aul_CreatePresetData("My Active Preset", aul_GetActiveMods(config_file, NULL));
			aul_PresetData* preset = NULL;
			preset = aul_CreatePresetData("My Preset", aul_GetSubscribedMods(workshop_dir, NULL));

			int success = 1;
			int mod_index = 0;
			for (mod_index=0; mod_index<preset->modCount; mod_index++) {
				if (strcmp(preset->mods[mod_index].workshop_id, argv[2]) == 0) {
					
					aul_ModData *new_mod = aul_CreateModData(preset->mods[mod_index].mod_name,
															preset->mods[mod_index].workshop_id,
															preset->mods[mod_index].workshop_url);
					success = aul_AddModToPreset(active_preset, *new_mod);
					aul_SetActiveMods(config_file, workshop_dir, active_preset->mods, active_preset->modCount);
					break;
				}
			}
			switch(success) {
				case 1:
					printf(
						"Mod not present in subscribed mods, subscribe and download this mod here:\n"
						"https://steamcommunity.com/sharedfiles/filedetails/?id=%s\n",
						argv[2]
						);
					break;
				case 0:
					printf("Successfully added \'%s\' to active mods", preset->mods[mod_index].mod_name);
					break;

				case -1:
					printf("Mod already active, nothing to do\n");
					break;
			}	
			aul_DestroyPresetData(active_preset);
			aul_DestroyPresetData(preset);
		}
		else if (strcmp(argv[1], "remove") == 0) {
			if (argc < 3) {
				printf("%s: command requires an argument -- 'remove'\n", argv[0]);
				return 1;
			}
			int option;
			while((option = getopt(argc, argv, "R")) != -1) {
				switch(option) {
					case 'R':
						aul_SetActiveMods(config_file, workshop_dir, NULL, 0);
						printf("Removed all mods from active preset\n");
						return 0;
						break;
					case '?':

						printf("See \'help\' commnand for help\n");
						return 1;
						break;
				}
			}
			aul_PresetData* active_preset = NULL;
			active_preset = aul_CreatePresetData("My Active Preset", aul_GetActiveMods(config_file, NULL));

			if (aul_RemoveModFromPreset(active_preset, argv[2])) {
				printf("%s has already been removed or is not present\n", argv[2]);
			}
			else {
				aul_SetActiveMods(config_file, workshop_dir, active_preset->mods, active_preset->modCount);
				printf("%s has been successfully removed from active preset\n", argv[2]);
			}
			//aul_DestroyPresetData(active_preset);
		}
		else if (strcmp(argv[1], "launch") == 0) {
			aul_LaunchArma();
		}
		else {
			printf("%s: unknown command -- \'%s\'\n", argv[0], argv[1]);
			return 1;
		}
	}
	return 0;
}
